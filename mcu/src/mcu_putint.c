#include <stdio.h>
#include "mcu_putint.h"

static int put_digit(int digit) {
    if (digit >= 0 &&  digit < 10) {
        putchar(digit+'0');
        return 0;
    }
    return -1;
}

static int putdecP(int d) {
    if ( d >= 10) {
        int r = d%10;
        int q = d/10;
        putdecP(q);
        put_digit(r);
        return 0;
    }
    else {
        return put_digit(d);
    }
}

int putdec(int d) {
    if( d == -2147483648) {
        int r = d%10;
        int q = d/10;
        r = -r;
        putdec(q);
        return put_digit(r);
    }

    else if ( d < 0) {
        d = -d;
        putchar('-');
        return putdecP(d);
    }
    else {
        return putdecP(d);
    }
}
