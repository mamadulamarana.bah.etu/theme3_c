#define SIZE 12
int tsrc[SIZE], tdest[SIZE];

int copy_tab(int t_dest [], const int t_src [], int taille) {
    int i = 0;
    for (i=0; i<taille; i++) {
        t_dest[i] = t_src[i];
    }
    return i;
}

int copy_tab_P(int t_dest [], const int t_src [], int taille) {
        int i,j = 0;
    for (i=0; i<taille; i++) {
        if ( t_src[i] > 0) {
            t_dest[j++] = t_src[i];
        }
    }
    return j;
}

int copy_tab_char (char t_dest [], const char t_src[]) {
    int len_src =0;
    for (len_src = 0; t_src[len_src] != '\0'; len_src++) {
        t_dest[len_src] = t_src[len_src];
    }
    t_dest[len_src] = '\0';
    return len_src;
}