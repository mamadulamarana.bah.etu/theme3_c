#include <stdio.h>
#include "mcu_putint.h"

int wc (void) {
    char c;
    int i = 0;
    for (i = 0; (c=getchar() != '\n'); i++) {}
    return ++i;
}
