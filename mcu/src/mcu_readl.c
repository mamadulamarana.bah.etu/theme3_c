#include <stdio.h>
#include "mcu_readl.h"
#include "mcu_fatal.h"

int readl(char line []) {
    int i=0;
    char c;
    while (i<MAXLINE - 2 && (c=getchar()) != EOF && c!= '\n') {
        line [i++] = c;
    }
    if ( i >= MAXLINE - 2) {
        fatal(1 == 0, "trop longue", 1);
    }
    else if (c == '\n') {
        line [i++] = '\n';
        line [i] = '\0';
        return i;
    }

    else {
        return EOF;
    }
}