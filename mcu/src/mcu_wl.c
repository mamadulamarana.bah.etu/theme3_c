#include <stdio.h>
#include "mcu_putint.h"

int wl(void ) {
    char c;
    int nb_l, i = 0;
    for (i = 0; (c=getchar()) != EOF ; i++) {
        if (c == '\n') {
            nb_l++;
        }
    }
    return nb_l;
}

int main(void) {
    int i = wl();
    putdec(i);
    return 0;
}