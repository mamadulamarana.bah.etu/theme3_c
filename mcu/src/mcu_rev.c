#include <stdio.h>
#include "mcu_macros.h"
#include "mcu_putint.h"

int rev(char tab[]) {
    int len=0;
    int i = 0;
    for (len=0; tab[len] != '\0'; len++) {}

    for (i=0; i < len; i++) {
        tab [i] = tab[len - i];
    }
    return len;
}

int main(void) {
    char tab[] = "Mamadou";

    int j = rev(tab);
    putdec(j);
    for (j = 0; tab[j] != '\0'; j++) {
        putchar(tab[j] + '\0');
    }
    return 0;
}